import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Category } from 'src/assets/models';

import { environment } from 'src/environments/environment';
const baseAPI = environment.baseAPI;
 

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  private url = baseAPI + "api/categories";

  constructor(
    private http:HttpClient,
  ) { }

  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.url).pipe(
      map((data: any) => this.retrieveCategories(data))
    );
  }

  retrieveCategories(data: any): Category[] {
    if (Array.isArray(data)) {
      return data as Category[];
  }
  else {
      const users = (data["data"] as Category[]);
      return users;
  }
  }
}
