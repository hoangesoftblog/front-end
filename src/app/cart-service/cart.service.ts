import { Injectable } from '@angular/core';
import { Product } from 'src/assets/models';
import { environment } from 'src/environments/environment';
import { ProductService } from '../product-management/product.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items: Array<Product> = [];

  constructor(
    private productService: ProductService,
  ) {
    if (!environment.production) {
      this.productService.getProducts().pipe(

      ).subscribe(
        (products: Product[]) => {
          console.log("Fetch products", products);
          this.items.push(products[0]);
        }
      );
    }
  }

  getItems(): Array<Product> {
    return this.items;
  }

  get length(): number {
    return this.items.length;
  }

  addItem(product: Product) {
    this.items.push(product);
  }

  clearCart() {
    this.items = [];
    return this.items;
  }

  deleteItem(index: number) {
    this.items = this.items.filter((_, i) => i !== index);
  }
}
