import { Injectable } from '@angular/core';
import { LoginInfo } from 'src/assets/models';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private TOKEN_KEY = "token";
  private USER_KEY = "token";

  constructor() { }

  signOut(): void {

  }

  public saveToken(token: string): void {
    localStorage.setItem(this.TOKEN_KEY, token);
  }

  public getToken(): string | null {
    return localStorage.getItem(this.TOKEN_KEY);
  }

  public saveLoginInfo(loginInfo: LoginInfo): void {
    localStorage.setItem(this.USER_KEY, JSON.stringify(loginInfo));
  }

  public getLoginInfo(): LoginInfo | null {
    let temp = localStorage.getItem(this.USER_KEY);
    if (temp) return JSON.parse(temp) as LoginInfo;
    else return null;
  }

  public clearAll(): void {
    localStorage.removeItem(this.USER_KEY);
    localStorage.removeItem(this.TOKEN_KEY);
  }
}
