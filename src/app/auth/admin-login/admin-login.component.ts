import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserInfoService } from '../user-info.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  constructor(
    private http: HttpClient, 
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,

    // private loginService: AuthService,
    private loginService: UserInfoService,
  ) {  }

  loginForm = this.formBuilder.group({
    username: [""],
    password: [""],
  })

  
  ngOnInit(): void {
  }

  onSubmit() {
    // console.table(this.loginForm.value);
    this.login();
  }

  private login() {
    const {username, password} = this.loginForm.value;
    this.loginService.login(username, password)
    .pipe(
      
    )
    .subscribe((result: any) => {
      // Currently, result is a LoginInfo | undefined
      // But can be changed into String with JWT only
      console.log(result && (result.role === "Admin"));
      // if (result && (result.role === "Admin")) {
      //   this.router.navigate(["/admin/account-mgmt"]);
      // }

      // // Here, even we force navigate to /admin/account-mgmt,
      // // but it does not work, since UserInfoService.isLoggedIn still false
      // // => Still move back to /login
      this.router.navigate([this.loginService.redirectedURL ?? ""]);
      this.loginService.redirectedURL = undefined;
    })
  }
}
