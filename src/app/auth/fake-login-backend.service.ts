import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs';
import { LoginInfo, LoginInfoWithToken } from 'src/assets/models';
import { environment } from 'src/environments/environment';
const baseAPI = environment.baseAPI;


@Injectable({
  providedIn: 'root'
})
export class FakeLoginBackendService {
  constructor(
    private http: HttpClient,
  ) { }

  private url = baseAPI + "api/login";
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  
  login(username: string, password: string) {
    return this.http.get<LoginInfo>(this.url + `?username=${username}`).pipe(
      tap((data: any) => {
        console.log(data);
      }),
      map((data: any) => {
        return this.retrieveLoginInfos(data)
      }),
      tap((acc: LoginInfo[]) => {
        console.log("Account in DB:");
        console.table(acc);
      }),

      // Main logic is here
      // Must update to be correct
      map((accounts: LoginInfo[]): LoginInfoWithToken | undefined => {
        if (!accounts || accounts.length === 0 || accounts.length > 2) {
          return undefined;
        }
        let account = accounts[0];
        if (account.password === password) 
          return {...account, token: "token-" + JSON.stringify(account),};
        else return undefined;    
      }),
    );
  }

  private retrieveLoginInfos(data: any): LoginInfo[] {
    return (data["data"] as LoginInfo[]);
  }
}
