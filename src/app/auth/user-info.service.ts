import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay, map, Observable, of, tap } from 'rxjs';
import { LoginInfo, LoginInfoWithToken } from 'src/assets/models';
import { FakeLoginBackendService } from './fake-login-backend.service';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {
  get isLoggedIn(): boolean {
    return this.tokenService.getToken() !== null;
  };

  get currentlyLoggedIn(): LoginInfo | null {
    return this.tokenService.getLoginInfo();
  }

  constructor(
    private http: HttpClient,
    private tokenService: TokenService,
    private fakeBackend: FakeLoginBackendService,
  ) { }

  login(username?: string, password?: string): Observable<LoginInfo | undefined> {
    // if ([...arguments].every(e => e)) 
      return this.realLogin(username!, password!);
    // else return of({username: "hoang", password: "truong"}).pipe(
    //   delay(1000),
    //   tap((val: Boolean) => {
    //     this.isLoggedIn = true;
    //   })
    // )
  }

  // TODO: This function is implemented SUPER WRONG!!!
  private realLogin(username: string, password: string): Observable<LoginInfoWithToken | undefined> {
    return this.fakeBackend.login(username, password).pipe(
      tap((val: LoginInfoWithToken | undefined) => {
        if (val === undefined) {
          
        }
        else {
          const {token, ...info} = val;
          this.tokenService.saveToken(val.token);
          this.tokenService.saveLoginInfo(info);
          console.log(this.tokenService.getToken());
        }
      })
    );
  }

  // getCurrentLoginInfo(): Observable<LoginInfo> {
  //   return of(this.loggedInfo!);
  // }

  logout(): Observable<any> {
    // this.isLoggedIn = false;
    // this.loggedInfo = undefined;
    return of(this.tokenService.clearAll());
  }


  redirectedURL?: string;
}
