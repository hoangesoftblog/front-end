import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UserInfoService } from './user-info.service';

@Injectable({
    providedIn: 'root'
})
export class CurrentlyLoggedInAuthGuard implements CanActivate {
    constructor(
        private loginService: UserInfoService,
        private router: Router,
    ) {

    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        console.log("Guard");
        console.log(this.loginService);


        if (this.loginService.isLoggedIn) {
            return this.getRedirect(this.loginService.redirectedURL ?? "", this.loginService.currentlyLoggedIn?.role ?? "User")
            // return this.router.parseUrl("/admin")
        }
        else {
            return true;
        }
    }

    getRedirect(path: string, role: string): boolean | UrlTree {
        if (path.includes("/admin")) {
            if (role === "Admin") {
                return true;
            }
            else {
                return this.router.parseUrl("/404");
            }
        } else {
            return true;
        }
    }

}
