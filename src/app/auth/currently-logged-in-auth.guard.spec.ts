import { TestBed } from '@angular/core/testing';

import { CurrentlyLoggedInAuthGuard } from './currently-logged-in-auth.guard';

describe('CurrentlyLoggedInAuthGuard', () => {
  let guard: CurrentlyLoggedInAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CurrentlyLoggedInAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
