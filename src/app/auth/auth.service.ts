import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { UserInfoService } from './user-info.service';


/**
 * @deprecated
 * This class should focus to perform login/logout, retrieve currently logged-in info, etc.  
 * But I accidentally put these all functionality into UsersInfoService :)
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token?: string;

  constructor(
    private http: HttpClient,
    private userInfoService: UserInfoService,
  ) { }

  login(username: string, password: string,): Observable<any> {
    return this.userInfoService.login(username, password)
    .pipe(
      tap((token: any) => {this.token = (token as string);}),
    );
  }
}
