import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { CommonComponentModule } from '../common-component/common-component.module';
import { ReactiveFormsModule } from '@angular/forms';
import { UserInfoService } from './user-info.service';



@NgModule({
  declarations: [
    AdminLoginComponent,
  ],
  imports: [
    CommonModule,
    CommonComponentModule,
    ReactiveFormsModule,
  ],
  // providers: [
  //   UserInfoService,
  // ]
})
export class AuthModule { }
