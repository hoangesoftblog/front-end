import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TokenService } from "./token.service";


@Injectable({
    providedIn: "root"
})
export class AuthInterceptor implements HttpInterceptor {
    constructor(private token: TokenService) {
    }

    private TOKEN_HEADER_KEY = "Authorization";
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.token.getToken();
        let authReq = req;
        if (token !== null) {
            authReq = req.clone({headers: req.headers.set(this.TOKEN_HEADER_KEY, "Bearer " + token)});
        }

        return next.handle(authReq);
    }
}