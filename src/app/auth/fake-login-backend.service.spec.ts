import { TestBed } from '@angular/core/testing';

import { FakeLoginBackendService } from './fake-login-backend.service';

describe('FakeLoginBackendService', () => {
  let service: FakeLoginBackendService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FakeLoginBackendService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
