import { Injectable } from '@angular/core';
import { InMemoryDbService, RequestInfo, ResponseInterceptor, ResponseOptions } from 'angular-in-memory-web-api';
import { Observable } from 'rxjs';
import { Gender, Role, User, LoginInfo, Voucher, Product } from '../assets/models';

import { users, login, products, categories, vouchers, orders } from 'src/assets/dataset';

@Injectable({
    providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        return { users, login, products, categories, vouchers, orders };
    }

    genId<T extends {id: number}>(myTable: T[]): number {
        return myTable.length > 0 ? Math.max(...myTable.map(t => t.id)) + 1 : 1;
    }

    // responseInterceptor(res: ResponseOptions, ri: RequestInfo): ResponseInterceptor {
    //     console.log("call responseInterceptor");

    //     return res;
    //   }

    constructor() { }
}
