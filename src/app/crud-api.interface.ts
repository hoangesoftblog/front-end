import { Observable } from "rxjs";

export interface CRUDInterface<T extends {id: number}> {
    delete(id: number): Observable<void>;
    update(obj: T): Observable<void>;

    get(term?: string, sortBy?: string, order?: "asc" | "desc" | "none", page?: number, limit?: number): Observable<T[]>;
    add(obj: T): Observable<T>;
    getOne(id: number): Observable<T>;
}