import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { ReactiveFormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MessageComponent } from './message/message.component';

import { AccountManangementModule } from './account-manangement/account-manangement.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonComponentModule } from './common-component/common-component.module';
import { ProductManagementModule } from './product-management/product-management.module';
import { AuthModule } from './auth/auth.module';
import { AuthInterceptor } from './auth/auth.interceptor';
import { VoucherManagementModule } from './voucher-management/voucher-management.module';
import { OrderMgmtModule } from './order-mgmt/order-mgmt.module';
import { StoreManagementModule } from './store-management/store-management.module';
import { environment } from 'src/environments/environment';
import { AdminModule } from './admin/admin.module';
import { UserModule } from './user/user.module';


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    // AdminLoginComponent,
    MessageComponent,

    // ModalComponent,

    // SearchComponent,
    // PaginationComponent,

    // UpdateUserComponent,
    // AddUserComponent,
    // AccountManangementComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,

    UserModule,
    
    AdminModule,
    AccountManangementModule,
    ProductManagementModule,
    NgbModule,
    CommonComponentModule,
    AuthModule,


    // Always put last
    VoucherManagementModule,
    OrderMgmtModule,
    StoreManagementModule,
    // HttpClientInMemoryWebApiModule must be imported after HttpClientModule
    environment.production ? [] : HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, {dataEncapsulation: true, delay: 500}
    ),
    AppRoutingModule,

  ],
  providers: [
    Title,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true,}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
