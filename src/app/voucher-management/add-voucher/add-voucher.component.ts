import { Component, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { map, Observable, tap } from 'rxjs';
import { ProductService } from 'src/app/product-management/product.service';
import { Product, Voucher } from 'src/assets/models';
import { VoucherService } from '../voucher.service';

@Component({
    selector: 'app-add-voucher',
    templateUrl: './add-voucher.component.html',
    styleUrls: ['./add-voucher.component.css']
})
export class AddVoucherComponent implements OnInit {
    // @Output()
    @Input() voucher?: Voucher;

    form: FormGroup;
    products!: Product[];

    constructor(
        private activeModal: NgbActiveModal,
        private fb: FormBuilder,
        private productService: ProductService,
    ) {
        this.form = this.fb.group({
            code: ["ABC", [Validators.required]],
            limit: [0, [Validators.required, Validators.min(0)]],
            minProductsRequired: [1, [Validators.required, Validators.min(0)]],

            start: [new Date(), [Validators.required]],
            end: [new Date(), [Validators.required]],
        
            maxPercentage: [0, [Validators.required, Validators.max(100), Validators.min(0)]],
            items: this.fb.array([]),
            maxDiscountInVND: [0, [Validators.required, Validators.min(0)]]
        },
            {validators: this.voucherDateValidator}
        );

        this.productService.getProducts().pipe(
            map((data: any) => {
                return this.productService.retrieveMultiple(data);
            }),
        ).subscribe(
            (products: Product[]) => {
                console.log("Fetch products", products);
                this.products = products;
            }
        );

        this.form.valueChanges.subscribe(
            () => {console.log(this.form.errors)}
        )
    }

    private voucherDateValidator(control: AbstractControl): ValidationErrors | null {
        const start = control.get("start");        
        const end = control.get("end");

        // True
        console.log(start?.value instanceof Date)   

        // The value are in String format
        console.log("Date start is:", start?.value);
        console.log("Date end is:", end?.value);
        
        // Return -1 (<), 0 (=), 1 (>)
        const cmp = end?.value >= start?.value;
        console.log("Comparison", cmp);
        return cmp ? null : {
            startLargerThanEnd: true
        }
    }

    ngOnInit(): void {
        if (this.voucher) {
            let tempVoucher = this.parseVoucherForEdit(this.voucher);
            this.form.patchValue({...tempVoucher});
            tempVoucher.items.forEach((p: Product) => {
                this.items.push(this.generateItemFormGroup(p));
            });
            console.log("Editing voucher", this.form.value);
        }

        // Make the "code" into uppercase
        this.form.get("code")?.valueChanges.subscribe((val: string) => {
            this.form.get("code")?.setValue(val.toUpperCase(), {
                emitEvent: false,
            })
        });

        // this.productService.getProducts().pipe(
        //     map((data: any) => {
        //         let products = this.productService.retrieveProducts(data);
        //         console.log("Fetch products", products);
        //         this.products = products;
        //     }),
        // );
    }


    private returnVoucherToStore(value: any): Voucher {
        let voucher = {...value, usageLeft: value["limit"],} as Voucher;
        return voucher;
    }

    private parseVoucherForEdit(v: any): Voucher {
        let start, end: any;
        ({start, end} = v);
        if (start) {
            if (typeof start === "string") {
                start = new Date(start);
            } else if (typeof start === "number") {
                // Need microseconds here
                start = new Date(start);
            } else {
                start = new Date();
            }
        } else {
            start = new Date();
        }

        if (end) {
            if (typeof end === "string") {
                end = new Date(end);
            } else if (typeof end === "number") {
                end = new Date(end);
            } else {
                end = new Date();
            }
        } else {
            end = new Date();
        }
        
        

        let voucher = {...v, 
            start, end,
        };
        return voucher;
    }


    onSubmit(): void {
        console.log("Error:", this.form.get("maxDiscountInVND")?.errors);
        console.log("Value:", this.form.value);
        console.log("Form valid", this.form.valid);

        if (this.form.valid) {
            // // The form value here has a key "minProducts" instead of "minProductsRequired",
            // // To match the Voucher shape without changing the HTML, the "stupid "replace is turned into function.
            let voucher = this.returnVoucherToStore(this.form.value);
            console.log("Voucher:", JSON.stringify(voucher));
            // console.log(typeof(voucher.end));
            this.activeModal.close(voucher);

            // if (voucher) {
            //     this.voucherService.addVoucher(voucher).pipe(

            //     )
            //         .subscribe((d: any) => {
            //             let tempVoucher = this.voucherService.retrieveSingle(d);
            //             console.log("Voucher added:", tempVoucher);
            //             this.activeModal.close(tempVoucher);
            //         })
            // }
        }
    }

    get items() {
        let items = this.form.get("items") as FormArray;
        return items;
    }

    private generateItemFormGroup(item?: Product): AbstractControl {
        let formGroup = this.fb.control(
            null, [Validators.required],
        );
        if (item) {
            formGroup.patchValue(item);
        }

        return formGroup;
    }

    addItem() {
        let formGroup = this.generateItemFormGroup();
        this.items.push(formGroup);
    }

    deleteItem(i: number): void {
        this.items.removeAt(i);
    }

    cancel() {
        this.activeModal.dismiss("Add voucher - cancel.");
    }

    compareProduct(p1: Product, p2: Product): boolean {
        return (p1 && p2) ? p1.id === p2.id : p1 === p2; 
    }
}
