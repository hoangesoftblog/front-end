import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, tap } from 'rxjs';
import RetrieveData from 'src/assets/common-interface';
import { vouchers } from 'src/assets/dataset';
import { Voucher } from 'src/assets/models';


import { environment } from 'src/environments/environment';
const baseAPI = environment.baseAPI;

@Injectable({
  providedIn: 'root'
})
export class VoucherService implements RetrieveData {
  url = baseAPI + "api/vouchers";
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };


  constructor(
    private http: HttpClient,
  ) { }

  getVouchers(term?: string): Observable<Voucher[]> {
    // If all arguments are null, then just fetch all the database
    if ([...arguments].every(e => !e)) {
      return this.http.get<Voucher[]>(this.url).pipe(
        map((data: any) => this.retrieveMultiple(data))
      );
    }

    // Else, try to search
    let vouchersObservable = of<Voucher[]>(vouchers);
    if (term) {
      vouchersObservable = vouchersObservable.pipe(
        tap((products: Voucher[]) => { console.log("Get products, length:", products.length) }),
        map((products: Voucher[]) => products.filter((product) => (Object.values(product).some((val) => ((String(val).toLowerCase()).includes(term.toLowerCase())))))),
      )
    }

    return vouchersObservable;
  }


  addVoucher(voucher: Voucher): Observable<Voucher> {
    return this.http.post<Voucher>(this.url, voucher, this.httpOptions)
    .pipe(
      map((val: any) => this.retrieveSingle(val))
    )
  };


  updateVoucher(voucher: Voucher): Observable<Voucher> {
    return this.http.put<Voucher>(this.url, voucher, this.httpOptions)
    .pipe(
      map((data: any) => this.retrieveSingle(data))
    );
  }

  deleteVoucher(id: number): Observable<void> {
    return this.http.delete<void>(this.url + `/${id}`, this.httpOptions)
    .pipe()
    ;
  }
  


  retrieveMultiple(data: any): Voucher[] {
    if (Array.isArray(data)) {
      return data;
    }
    else {
      const users = (data["data"] as Voucher[]);
      return users;
    }
  }
  retrieveSingle(data: any): Voucher {
    if (data?.["data"]) return data["data"];
    else return data;
  }
}
