import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VoucherManagementRoutingModule } from './voucher-management-routing.module';
import { VoucherManagementComponent } from './voucher-management.component';
import { VoucherService } from './voucher.service';
import { CommonComponentModule } from '../common-component/common-component.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AddVoucherComponent } from './add-voucher/add-voucher.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { VoucherDetailComponent } from './voucher-detail/voucher-detail.component';
import { DateValueAccessorModule } from 'angular-date-value-accessor';


@NgModule({
  declarations: [
    VoucherManagementComponent,
    AddVoucherComponent,
    VoucherDetailComponent,
  ],
  imports: [
    CommonModule,
    CommonComponentModule,
    ReactiveFormsModule,
    VoucherManagementRoutingModule,
    DateValueAccessorModule,
  ],
  providers: [
    VoucherService,
    NgbActiveModal,
  ]
})
export class VoucherManagementModule { }
