import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Voucher } from 'src/assets/models';
import { RowAction, TableColumn } from '../common-component/table/table.component';
import { ProductService } from '../product-management/product.service';
import { AddVoucherComponent } from './add-voucher/add-voucher.component';
import { VoucherDetailComponent } from './voucher-detail/voucher-detail.component';
import { VoucherService } from './voucher.service';

@Component({
    selector: 'app-voucher-management',
    templateUrl: './voucher-management.component.html',
    styleUrls: ['./voucher-management.component.css']
})
export class VoucherManagementComponent implements OnInit {
    constructor(
        private voucherService: VoucherService,
        private modalService: NgbModal,
    ) { }

    vouchers: Voucher[] = [];

    get dbVouchers(): Voucher[] {
        return this.vouchers.slice(this.page * this.limit, (this.page + 1) * this.limit);
    }

    private limit = 10;

    page: number = 0;
    get lastPage(): number {
        if (!this.vouchers.length) return 0;
        else return Math.floor((this.vouchers.length - 1) / this.limit);
    }
    onPageChange(page: number): void {
        this.page = page;
        this.setVouchers(this.vouchers);
    }

    columns: TableColumn[] = [
        { title: "Code", property: "code" },
        { title: "Start", property: "start" },
        { title: "End", property: "end" },
        { title: "Remaining", property: "usageLeft" },
        { title: "Discount up to (%)", property: "maxPercentage" },
        { title: "Discount up to (VND)", property: "maxDiscountInVND" },
        { title: "No. products required", property: "minProductsRequired" },
    ];

    rowActions: RowAction[] = [
        {
            title: "Detail",
            action: this.openVoucherDetail.bind(this),
        },
        {
            title: "Edit",
            action: this.openEditVoucher.bind(this),
        }
    ];

    openVoucherDetail(voucher: Voucher): void {
        let ref = this.modalService.open(VoucherDetailComponent, { size: "lg" });
        ref.componentInstance.voucher = voucher;
    }

    openEditVoucher(voucher: Voucher): void {
        let ref = this.modalService.open(AddVoucherComponent, { size: "lg" });
        ref.componentInstance.voucher = voucher;
        ref.result.then(
            (voucher: Voucher) => {
                if (voucher) {
                    for (let i = 0; i < this.vouchers.length; i++) {
                        if (voucher.id === this.vouchers[i].id) {
                            console.log("Update product, match with id:", this.vouchers[i].id)
                            this.vouchers[i] = voucher;
                            break;
                        }
                    }

                    this.setVouchers(this.vouchers);
                }
            },
            (reason: ModalDismissReasons | string) => {

            }
        );
    }

    openAddVoucher() {
        this.modalService.open(AddVoucherComponent, { size: "lg" }).result.then(
            (value: Voucher) => {
                this.voucherService.addVoucher(value)
                .subscribe(
                    (voucher: Voucher) => {
                        console.log("Add voucher:", voucher);
                        this.vouchers.push(voucher);
                    }
                );
            },
            (reason: ModalDismissReasons | string) => {
                console.log("Voucher add - close:", reason);
            }
        );
    }


    ngOnInit(): void {
        this.voucherService.getVouchers().pipe(

        ).subscribe((data: Voucher[]) => {
            this.setVouchers(this.voucherService.retrieveMultiple(data));
        });

        // this.openAddVoucher();
    }

    setVouchers(vouchers: Voucher[]) {
        this.vouchers = vouchers;
    }

    search(term: string): void {
        this.voucherService.getVouchers(term)
            .pipe(

            )
            .subscribe((vouchers: Voucher[]) => {
                this.page = 0;
                this.setVouchers(this.voucherService.retrieveMultiple(vouchers));
            });
    }

    refresh() {
        this.voucherService.getVouchers().pipe(

        )
            .subscribe((data: any) => {
                this.page = 0;
                this.setVouchers(this.voucherService.retrieveMultiple(data));
            })
    }


    selection!: SelectionModel<any>;
    get allSelected(): Voucher[] {
        return this.selection?.selected ?? [];
    }
    setAllSelected(selection: SelectionModel<any>): void {
        this.selection = selection;
        // console.log(this.allSelectedProducts);
    }
    openDeleteDialog(modal: any): void {
        this.modalService.open(modal).result.then(
            (val) => {
                console.log("Delete user - close. Confirm delete");
                this.selection.selected.forEach((voucher: Voucher) => {
                    this.voucherService.deleteVoucher(voucher.id)
                    .pipe(
                        // catchError((err, caught: Observable<User>) => {return of([])})
                    )
                    .subscribe(
                        // user in this is null
                        (tempUser: void) => {
                            console.log("Delete voucher,", tempUser);
                            this.setVouchers(this.vouchers.filter(v => v.id !== voucher.id));
                        }
                    )
                });
                this.selection.clear();
            },
            (err) => {
                
            }
        )
    }
}
