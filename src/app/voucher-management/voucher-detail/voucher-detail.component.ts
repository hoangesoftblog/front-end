import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Product, Voucher } from 'src/assets/models';

@Component({
  selector: 'app-voucher-detail',
  templateUrl: './voucher-detail.component.html',
  styleUrls: ['./voucher-detail.component.css']
})
export class VoucherDetailComponent implements OnInit {
  @Input() voucher!: Voucher;

  constructor(
    private activeModal: NgbActiveModal,
    private router: Router,
  ) { }

  ngOnInit(): void {
    console.log("Voucher details:", this.voucher);
  }

  cancel(): void {
    this.activeModal.dismiss("Voucher detail - close");
  }

  closeModal(): void {
    // this.router.navigate(["/product", item.id]);
    this.activeModal.dismiss();
  }

}
