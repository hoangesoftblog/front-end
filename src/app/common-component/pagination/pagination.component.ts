import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, SimpleChanges } from '@angular/core';


@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input() page: number = 0;
  /**
   * The last page, counting from 0
   */
  @Input("lastPage") lastPage!: number;
  @Output() pageChange = new EventEmitter();
  @Input() limit? = 10;
  @Input() numOfElements = 0;

  constructor() { }

  ngOnInit(): void {
    this.pageChange.emit(this.page);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

  setPage(page: number) {
    this.page = page;
    this.pageChange.emit(this.page);
  }
}
