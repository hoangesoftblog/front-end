export interface IPagination {
    readonly lastPage: number;
    page: number;
    limit: number | undefined;
    numOfElements: number;
    pageChange: (page: number) => any
}