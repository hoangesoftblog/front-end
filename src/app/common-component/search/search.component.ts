import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @Output() resultEmitter = new EventEmitter();

  search: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.search = formBuilder.group({
      search: [""]
    })
   }

  ngOnInit(): void {
  }
  
  submit() {
    let input = this.search.get("search")?.value ?? "";
    console.log("Search component:", input);
    this.resultEmitter.emit(input);
  }
}
