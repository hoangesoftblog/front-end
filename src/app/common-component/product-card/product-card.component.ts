import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CartService } from 'src/app/cart-service/cart.service';
import { Product } from 'src/assets/models';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {
  @Input() product!: Product;
  @Input() class: any;

  @Output() addProductEmitter = new EventEmitter();

  constructor(
    private cartService: CartService,
  ) { }

  ngOnInit(): void {
  }

  addProduct(): void {
    alert("Product clicked added.");
    this.cartService.addItem(this.product);
    // this.addProductEmitter.emit();
  }

}
