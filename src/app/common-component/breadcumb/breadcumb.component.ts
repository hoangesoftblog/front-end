import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, Observable } from 'rxjs';
import { Breadcrumb, BreadcrumbService } from './breadcumb.service';

@Component({
  selector: 'app-breadcumb',
  templateUrl: './breadcumb.component.html',
  styleUrls: ['./breadcumb.component.css']
})
export class BreadcumbComponent implements OnInit {
  breadcrumbs$: Observable<Breadcrumb[]>;

  currentURL?: string;

  constructor(
    private readonly breadcrumbService: BreadcrumbService,
    private router: Router,
  ) {
    this.breadcrumbs$ = breadcrumbService.breadcrumbs$;
    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
    )
    .subscribe((event) => { 
      this.currentURL = this.router.url; 
    });
  }

  ngOnInit(): void {
  }

}
