import { DecimalPipe } from "@angular/common";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: "vnd"})
export class VNDCurrencyPipe implements PipeTransform {
    transform(value: number | string, ): string | null {
        // en-us is default locale for DecimalPipe, so just let is there
        let decimalPipe = new DecimalPipe("en-us");
        let result = decimalPipe.transform(value, "1.0-3");
        return result ? result + " đ" : null;
    }
}