import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationComponent } from './pagination/pagination.component';
import { SearchComponent } from './search/search.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TableComponent } from './table/table.component';
import { BreadcumbComponent } from './breadcumb/breadcumb.component';
import { TableHeaderComponent } from './table/table-header/table-header.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from "@angular/material/input";
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from "@angular/material/radio";
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from "@angular/material/datepicker";
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BreadcrumbService } from './breadcumb/breadcumb.service';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { ProductCardComponent } from './product-card/product-card.component';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatBadgeModule } from '@angular/material/badge';
import { VNDCurrencyPipe } from './customPipe/vnd-pipe';
import { MatChipsModule } from '@angular/material/chips';





const MaterialModulesWithoutWrapper = [
  // Angular Material Module
  MatPaginatorModule,
  MatSortModule,
  MatIconModule,
  MatPaginatorModule,
  MatToolbarModule,

  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatSortModule,
  MatRadioModule,
  MatSelectModule,
  MatDialogModule,
  MatButtonModule,
  MatCardModule,
  MatListModule,
  MatGridListModule,
  MatBadgeModule,
  MatChipsModule,

  // // Datepicker module
  MatDatepickerModule,
  // DateAdapter for MatDatepicker
  MatNativeDateModule,
]

const MaterialModulesWithWrapper = [
  MatTableModule,  
]

const OtherImports: any[] = [
  // VNDCurrencyPipe,
]

@NgModule({
  declarations: [
    PaginationComponent,
    SearchComponent,
    TableComponent,
    BreadcumbComponent,
    TableHeaderComponent,
    TopBarComponent,
    DatepickerComponent,
    ProductCardComponent,


    VNDCurrencyPipe,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    BrowserModule,
    ...MaterialModulesWithWrapper,
    ...MaterialModulesWithoutWrapper,
    ...OtherImports,
  ],
  exports: [
    PaginationComponent,
    SearchComponent,
    TableComponent,
    BreadcumbComponent,
    TopBarComponent,
    DatepickerComponent,
    ProductCardComponent,

    ...MaterialModulesWithoutWrapper,
    ...OtherImports,

    VNDCurrencyPipe,
  ],
  providers: [
    BreadcrumbService,
  ]
})
export class CommonComponentModule { }
