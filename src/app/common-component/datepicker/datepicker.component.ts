import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.css']
})
export class DatepickerComponent implements OnInit {
  @Output() emitter = new EventEmitter();

  date: FormControl;
  constructor(
    private fb: FormBuilder,
  ) {
    this.date = this.fb.control(null, [Validators.required]);
    this.emitter.emit(this.date.value);
  }

  ngOnInit(): void {
  }

  
}
