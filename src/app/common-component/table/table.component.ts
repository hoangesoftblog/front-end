import { SelectionChange, SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() columns: TableColumn[] = [];
  @Input("data") data: any[] = [];
  @Input() rowActions: RowAction[] = [];

  @Output() sortEmitter = new EventEmitter<SortEvent>();
  @Output() selectedEmitter = new EventEmitter();


  displayColumns!: string[];

  constructor() { }

  ngOnInit(): void {
    // I want to shuffle the order of the properties of the columns to see the effect
    // The header still follows the data in the row

    // this.displayColumns = this.shuffleArray(this.columns.map((e) => e["property"]));
    this.displayColumns = this.columns.map((e) => e["property"]);

    // // Table does not get init again for input changes
    // console.log("Table get inited again.")

    if (this.rowActions.length) {
      this.displayColumns.push("action");
    }
    if (this.enableRowSelect) {
      this.displayColumns.push("select");
    }

    // console.log(this.displayColumns);

    this.selection.changed.subscribe(
      (changes) => {
        // To get the selected elements here,
        // call changes.source.selected
        
        // But when select all, multiple changes are fired, each changes are for 1 element only
        // in the changes.added[0]
        console.log("Changes:", changes);
        this.selectedEmitter.emit(this.selection);
        console.log("Selection Model:", this.selection);
      }
    );
  }

  
  // @ViewChild(MatTable) table?: MatTable<any>;
  ngOnChanges(changes: SimpleChanges): void {
    // console.log("Table changes:", changes);

    if (this.allSelected) {
      this.selection.clear();

      let new_data = changes["data"].currentValue as any[];
      new_data.forEach(row => this.selection.select(row));
    }

    if (changes["data"]) {
      // if (this.table) this.table.renderRows();
    }
  }


  private shuffleArray(arr: any[]): any[] {
    return arr.slice().sort(() => Math.random() - 0.5);
  }



  @Input() enableRowSelect: boolean = true;
  selection = new SelectionModel<any>(true, []);
  allSelected: boolean = false;
  isAllSelected(): boolean {
    return this.allSelected;
  }

  masterToggle() {
    this.allSelected = !this.allSelected;
  }

  masterToggleWithAction() {
    this.masterToggle();
    this.isAllSelected() ? this.data.forEach((row: any) => this.selection.select(row)) : this.selection.clear();
    console.log(this.allSelected);
  }

  toggleRow(row: any) {
    this.selection.toggle(row);
    this.allSelected = false;
    
  }


  // data = new MatTableDataSource<any>([]);

  // @ViewChild(MatSort) sort!: MatSort;

  ngAfterViewInit(): void {
    // this.data = new MatTableDataSource(this.tempData);
    // this.table.renderRows();
    // this.data.sort = this.sort;
  }

  createSortEvent(sortState: Sort) {
    console.log(sortState);
    let sortEvent: SortEvent = {col: sortState.active, order: sortState.direction};
    this.sortEmitter.emit(sortEvent);
  }
}


/**
 * title: The display name of a column
 * property: The property of a row
 */
export type TableColumn = {
  title: string,
  property: string,
  displayAs?: (val: any) => any,
  sortable?: boolean,
}

export type RowAction = {
  title: string,
  action: (element?: any) => any,
}

export type SortEvent = {
  col: string,
  order: "asc" | "desc" | "",
}