import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateUserComponent } from './update-user/update-user.component';
import { AddUserComponent } from './add-user/add-user.component';
import { AccountManangementComponent } from './account-manangement.component';
import { UsersService } from './users.service';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from '../in-memory-data.service';
import { AdminComponent } from '../admin/admin.component';
import { BrowserModule } from '@angular/platform-browser';
import { UserInfoService } from '../auth/user-info.service';
import { CommonComponentModule } from '../common-component/common-component.module';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';



@NgModule({
  declarations: [
    UpdateUserComponent,
    AddUserComponent,
    AccountManangementComponent,
    AccountDetailComponent,

    // TopBarComponent,  
    // BreadcumbComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    // HttpClientInMemoryWebApiModule.forRoot(
    //   InMemoryDataService, {dataEncapsulation: true}
    // ),
    BrowserModule,
    CommonComponentModule,
    
    MatFormFieldModule,
    MatInputModule,
  ],
  providers: [
    UsersService,
    UserInfoService,
    NgbActiveModal,
  ]
})
export class AccountManangementModule { }
