import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountManangementComponent } from './account-manangement.component';

describe('AccountManangementComponent', () => {
  let component: AccountManangementComponent;
  let fixture: ComponentFixture<AccountManangementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountManangementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountManangementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
