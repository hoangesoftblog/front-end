import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../../../assets/models';
import { UsersService } from '../users.service';

@Component({
    selector: 'app-add-user',
    templateUrl: './add-user.component.html',
    styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
    @Output() userEmitter = new EventEmitter<User>();

    private user: User | undefined = undefined;

    form: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private userService: UsersService,
        private activeModal: NgbActiveModal,

        // Angular Material UI
        private dialogRef: MatDialogRef<AddUserComponent>,
        @Inject(MAT_DIALOG_DATA) public data: User,
    ) {
        this.form = this.formBuilder.group({
            email: ["", [Validators.required, Validators.email]],
            name: ["", [Validators.required]],
            gender: ["Male", [Validators.required]],
            role: ["User", [Validators.required]],
            phone: [""],
        });

        console.log("Data", data as User);
        if (data) {
            this.user = data as User;
            this.form.patchValue(data);
        }
    }

    submit() {
        console.log("Add is value", this.form.valid);
        // console.log("Form value is: ", this.form.value);
        if (this.form.valid) {
            let finishedUser: any = { ...this.form.value };

            // Merge the ID from the existing user into finished user.
            if (this.user) {
                finishedUser = {...this.user, ...finishedUser,};
            }

            if (finishedUser) {
                // this.userService.addUser(finishedUser)
                //     .subscribe((user: User) => {
                //         // console.log(content);
                //         // let finishedUser = user["data"] as User;
                //         // // Return the user back to AccountManagementComponent
                //         // this.userEmitter.emit(finishedUser);

                //         // this.activeModal.close(user);
                //         this.dialogRef.close(user);
                //     })
                this.dialogRef.close(finishedUser);
            }
        }
        else {
            console.log(this.form.errors);
        }
    }

    cancel() {
        this.activeModal.dismiss("AddUser - cancel");

        this.dialogRef.close();
    }

    ngOnInit(): void {
        console.log("Add user show!")
    }


    get email(): FormControl {
        return this.form.get("email") as FormControl;
    }
    get name(): FormControl {
        return this.form.get("name") as FormControl;
    }
    get gender(): FormControl {
        return this.form.get("gender") as FormControl;
    }
    get role(): FormControl {
        return this.form.get("role") as FormControl;
    }
    get phone(): FormControl {
        return this.form.get("phone") as FormControl;
    }
}
