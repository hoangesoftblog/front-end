import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, filter, map, Observable, of, tap } from 'rxjs';
import { users } from 'src/assets/dataset';
// import { MessageService } from './message.service';
import { User } from '../../assets/models';

import { environment } from 'src/environments/environment';
const baseAPI = environment.baseAPI;


@Injectable({
    providedIn: 'root'
})
export class UsersService {
    url = baseAPI + "api/users";

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
      };

    constructor(
        private http: HttpClient,
        // private messageService: MessageService,
    ) { }


    /**
     * 
     * @param term Search keyword
     * @param sortColumn 
     * @param order 
     * @param page 
     * @param limit 
     * @returns 
     */
    getUsers(term?: string, sortColumn?: string, order?: "asc" | "desc", page?: number, limit?: number): Observable<User[]> {        
        // If all arguments are null, then just fetch all the database
        if ([...arguments].every(e => !e)) {
            return this.http.get<User[]>(this.url);
        }
        
        console.log("term:", term, typeof(term));
        // Else, try to search
        let usersObservable = of(users);
        if (term) {
            usersObservable = usersObservable.pipe(
                tap((users) => {console.log("Get users, length:", users.length)}),
                map((users) => users.filter((user) => (Object.values(user).some((val) => ((String(val).toLowerCase()).includes(term.toLowerCase())))))),                
            )
        }

        if (sortColumn) {
            usersObservable = usersObservable.pipe(
                map((users) => {
                    return users.slice()                
                    .sort((a: User, b: User): number => {
                        if (sortColumn && Object.keys(a).includes(sortColumn)) return (((a as any)[sortColumn] as string).localeCompare((b as any)[sortColumn] as string))
                        // Keeps in the same order
                        else return 1;
                    })
                })
            );
        }

        return usersObservable;
    }

    // sort(order: -1 | 1) {
    //     return function (column: string){
    //         return of(users.slice().sort((a: User, b: User): number => {
    //             // let cmp = 0;
    //             // if (typeof a[column] === "string") {
    //             //     if (a[column] === b[column]) cmp = 0;
    //             //     else if (a[column] > b[column]) cmp = 1;
    //             // }
    //             // return cmp;
    //             return ((a as any)[column] as string).localeCompare((b as any)[column]) * order;
    //         })).subscribe(users => users);
    //     }
    // }

    // getUsersNew(): Observable<{data: User[], end: boolean}> {
    //     return this.http.get<{data: User[], end: boolean}>(this.userURL);
    // }

    updateUser(user: User): Observable<User> {
        return this.http.put<User>(this.url, user, this.httpOptions).pipe(
        );
    }

    deleteUser(id: number): Observable<void> {
        const url = `${this.url}/${id}`;

        return this.http.delete<void>(url, this.httpOptions)
        .pipe(
            tap(_ => this.log(`deleted hero id=${id}`)),
        );
    }

    addUser(user: User): Observable<User> {
        return this.http.post<User>(this.url, user, this.httpOptions)
        .pipe(
            tap((val: User) => {console.log("In the add user:", val)}),
            map((val: any) => (val["data"] as User)),
        );
    }


    searchUsers(term: string, sortColumn: string = "name", order: "asc" | "desc" = "asc", page: number = 0, limit: number = 10): Observable<User[]> {
        // return of(
        //     users
        //         // .slice(page * limit, (page + 1) * limit)
        //         .filter((user) => {
        //             let vals = Object.values(user);
        //             console.log(vals);

        //             return Object.values(user).some((val) => ((String(val)).includes(term)))
        //         })
        //         .sort((a: User, b: User): number => {
        //             if (Object.keys(a).includes(sortColumn)) return (((a as any)[sortColumn] as string).localeCompare((b as any)[sortColumn] as string))
        //             // Keeps in the same order
        //             else return 1;
        //         })
        // );
        return this.getUsers(...arguments);
    }

    private log(message: string) {
        // this.messageService.add(`UserService: ${message}`);
        console.log(`UserService: ${message}`)
    }

    retrieveData(data: any): User[] {
        if (Array.isArray(data)) {
            return data;
        }
        else {
            const users = (data["data"] as User[]);
            return users;
        }
    }

}
