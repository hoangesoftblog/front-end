import { Component, Input, OnInit, OnChanges, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../../../assets/models';
import { UsersService } from '../users.service';

@Component({
    selector: 'app-update-user',
    templateUrl: './update-user.component.html',
    styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit, OnChanges {
    @Input() user: User | undefined;
    @Output() userEmitter = new EventEmitter<User>();

    form: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private userService: UsersService,
        private activeModal: NgbActiveModal,
    ) {
        this.form = this.formBuilder.group({
            email: [this.user?.email, [Validators.required]],
            name: [this.user?.name],
            gender: [this.user?.gender, [Validators.required]],
            role: [this.user?.role],
            phone: [this.user?.phone],
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        // this.form.patchValue({...this.user});

        console.log("Update form.");
        console.log(this.form.value);
    }

    ngOnInit(): void {
        // console.table(this.form.value);
        // In ng-bootstrap, requires this.
        this.form.patchValue({...this.user});
    }

    cancel(): void {
        // this.user = undefined;
        console.log(this.form.valid);

        // this.userEmitter.emit(undefined);
        this.form.reset();
        this.activeModal.dismiss("Update user - cancel.");
    }

    submit() {
        console.log(this.form.valid);

        if (this.form.valid) {
            // form.value does not have the ID for the updated user,
            // so do this to get both ID from this.user and others from this.form.value.
            // Can set manually, but don't want to
            let finishedUser = {...this.user, ...this.form.value};
            if (finishedUser){
                this.userService.updateUser(finishedUser)
                .subscribe((content) => {
                    // // Content does not have anything, it's here to test for sure
                    // console.log(content);

                    console.log("Submit");
                    // // Return the user back to AccountManagementComponent
                    // this.userEmitter.emit(finishedUser);
                    this.activeModal.close(finishedUser);
                })
            }
        }
    }

    
}
