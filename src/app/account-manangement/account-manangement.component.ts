import { Component, OnInit, TemplateRef } from '@angular/core';
import { User } from '../../assets/models';
import { UsersService } from './users.service';
import { catchError, map, Observable, of, tap } from 'rxjs';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UpdateUserComponent } from './update-user/update-user.component';
import { AddUserComponent } from './add-user/add-user.component';
import { RowAction, SortEvent, TableColumn } from '../common-component/table/table.component';
import { MatDialog } from '@angular/material/dialog';
import { SelectionModel } from '@angular/cdk/collections';


@Component({
    selector: 'app-account-manangement',
    templateUrl: './account-manangement.component.html',
    styleUrls: ['./account-manangement.component.css']
})
class AccountManangementComponent implements OnInit {
    // The users on view
    dbUsers: User[] = [];
    // The users in the DB is currently fetch
    users: User[] = [];
    selectedUser: User | undefined;
    constructor(
        private userService: UsersService,
        private modalService: NgbModal,
        // Material UI Dialog
        private dialog: MatDialog,
    ) { }

    columns: TableColumn[] = [
        { title: "Name", property: "name", sortable: true, },
        { title: "Gender", property: "gender", sortable: true, },
        { title: "Role", property: "role", sortable: true, },
        { title: "Phone", property: "phone", sortable: true, },
        { title: "Email", property: "email", sortable: true, },
    ]

    rowActions: RowAction[] = [
        { title: "Edit", action: this.openUpdateModal.bind(this), }
    ]

    enableAdd: boolean = false;
    enableDelete: boolean = false;
    enableEdit: boolean = false;

    sortColumn: string = "id";
    page: number = 0;
    private limit = 10;


    get lastPage(): number {
        if (!this.users.length) return 0;
        else return Math.floor((this.users.length - 1) / this.limit);
    }


    ngOnInit(): void {
        this.userService.getUsers()
            .pipe(
            )
            .subscribe((data: Object) => {
                this.setUsers(this.userService.retrieveData(data));
            });

        // this.openAddDialog();
    }

    onChangePage(page: number) {
        this.page = page;
        this.dbUsers = this.users.slice(page * this.limit, (page + 1) * this.limit);
    }

    refresh(): void {
        this.userService.getUsers()
            .pipe(
                // catchError((err: any, caught: Observable<User[]>) => ({})),
                tap((users) => {
                    console.log("Refresh page", users);
                })
            )
            .subscribe((d: any) => {
                // Reset the page
                this.page = 0;
                this.setUsers(this.userService.retrieveData(d))
            });
    }

    search(term: string): void {
        this.userService.searchUsers(term)
            .pipe(

            )
            .subscribe(
                (data: Object) => {
                    console.log("Getting search,", data);
                    // Reset the page
                    this.page = 0;
                    this.setUsers(this.userService.retrieveData(data));
                },
            )
    }

    sort(sortState: SortEvent) {
        const sortOrder = { asc: 1, desc: -1, "": 0 };
        const { col: column, order } = sortState;

        this.setUsers(this.users.slice().sort((a: User, b: User): number => {
            if (Object.keys(a).includes(column)) {
                return (((a as any)[column] as string).localeCompare((b as any)[column] as string) * (sortOrder[order]));
            }
            // If key not exists, just
            // keeps in the same order. :v
            else return 1;
        }));

        console.log(this.dbUsers);
    }

    // ascSort = this.userService.sort(1);
    // acscSortName = this.ascSort("Name");
    // descSort = this.userService.sort(-1);

    setSelectedUser(user: User | undefined) {
        this.selectedUser = user;

        console.log(this.selectedUser);
        console.table({ "Add": this.enableAdd, "Del": this.enableDelete, edit: this.enableEdit })
    }


    openAddDialog(): void {
        this.resetAllEnable();
        this.enableAdd = true;

        // let ref = this.modalService.open(AddUserComponent);
        // ref.result.then(
        //     (val) => {
        //         this.addUser(val as User);
        //     },
        //     (err) => {
        //         console.log("Modal AddUser error");
        //         console.log(err);
        //     }
        // )

        let ref = this.dialog.open(AddUserComponent);
        ref.afterClosed().subscribe(
            (val) => {
                if (typeof (val) === "object") {
                    let u = val as User;
                    this.userService.addUser(u).subscribe(
                        (user: User) => {
                            this.addUser(user);
                        }
                    )
                }
            }
        )
    }

    addUser(user: User): void {
        this.users.push(user);
        this.setUsers(this.users);
    }


    // // Tutorial for open component as modal
    // // https://medium.com/@izzatnadiri/how-to-pass-data-to-and-receive-from-ng-bootstrap-modals-916f2ad5d66e
    openUpdateModal(user: User) {
        // let ref = this.modalService.open(AddUserComponent);
        // ref.componentInstance.user = user;
        // ref.result.then(
        //     (val) => {
        //         this.updateUser(val as User);
        //     },
        //     (err) => {
        //         console.log("Modal updateUser error");
        //         console.log(err);
        //     }
        // )

        this.dialog.open(AddUserComponent, {
            data: user
        }).afterClosed().subscribe(
            (val) => {
                console.log("Value", val);
                if (typeof (val) === "object") {
                    let u = val as User;
                    this.userService.updateUser(u).subscribe(
                        () => {
                            this.updateUser(u);
                        }
                    )
                }
            }
        )

    }

    //// The updateUser output event handler
    //// will receives event object from UpdateUserArgument 
    //// - both when cancel editing or finish and submit

    //// The argument here is the value from UpdateUserComponent.emit
    //// In the template though, it must be declared as $event. :(

    // user param was User | undefined, as both Edit and Cancel eventEmitter on UpdateUserComponent 
    // was designed to use the same output event handler updateUser.
    // But after the update, now the Cancel emitter does not need to use the updateUser anymore
    // So function is updated
    updateUser(user: User) {
        console.log("In account management", user);
        // this.setSelectedUser(user);

        // Turn off Edit dialog
        this.resetAllEnable();
        this.enableEdit = false;

        // Replacing the user with newly updated one
        if (user) {
            for (let i = 0; i < this.users.length; i++) {
                if (user.id === this.users[i].id) {
                    console.log("Update user, match with id:", this.users[i].id)
                    this.users[i] = user;
                    break;
                }
            }
        }
        // Refresh the page
        this.setUsers(this.users);
    }


    openDeleteModal(modal: any) {
        this.modalService.open(modal).result.then(
            (val) => {
                console.log("Delete user - close. Confirm delete");
                this.confirmDelete(true); this.selection.clear();
            },
            (err) => {
                console.log("Delete user - closed. Cancel delete");
                this.confirmDelete(false);
            }
        )
    }



    private resetAllEnable() {
        this.enableAdd = false;
        this.enableEdit = false;
        this.enableDelete = false;
        this.setSelectedUser(undefined);
    }

    setUsers(users: User[]) {
        this.users = users;

        // Update the display users on screen as well
        // // this.dbUsers = this.users.slice(0, this.limit);
        this.dbUsers = this.users.slice(this.page * this.limit, (this.page + 1) * this.limit);
    }



    confirmDelete(confirm: boolean) {
        if (confirm) {
            // let user: User = this.selectedUser!;
            // // this.users = this.users.filter(u => u.id !== user.id);
            this.selection.selected.forEach((user: User) => {
                this.userService.deleteUser(user.id)
                .pipe(
                    // catchError((err, caught: Observable<User>) => {return of([])})
                )
                .subscribe(
                    // user in this is null
                    (tempUser: void) => {
                        console.log("Delete user,", tempUser);
                        this.setUsers(this.users.filter(u => u.id !== user.id));
                    }
                )
            })
        }

        this.resetAllEnable();
        this.enableDelete = false;
    }


    selection!: SelectionModel<any>;
    get allSelected(): User[] {
        return this.selection?.selected ?? [];
    }
    setAllSelected(selection: SelectionModel<any>): void {
        this.selection = selection;
        // console.log(this.allSelectedProducts);
    }

}

export { AccountManangementComponent };

// class ExtendAccountManagementModule extends AccountManangementComponent {
//     openModal(content: any) {

//     }
// }
// export {ExtendAccountManagementModule as AccountManangementComponent};

