import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index.component';
import { CommonComponentModule } from 'src/app/common-component/common-component.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    IndexComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    CommonComponentModule,
  ]
})
export class IndexModule { }
