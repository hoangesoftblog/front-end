import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/cart-service/cart.service';
import { IPagination } from 'src/app/common-component/pagination/pagination-interface';
import { ProductService } from 'src/app/product-management/product.service';
import { Product } from 'src/assets/models';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  items: Product[] = [];
  page: number = 0;
  limit: number = 12;
  get lastPage(): number {
    return (!this.items.length) ? 0 : Math.floor((this.items.length - 1) / this.limit);
  }
  get numOfElements(): number {return this.items.length};
  pageChange(page: number): void {
    this.page = page;
  }
  

  colNum: number = 4;
  get cols() {
    return [...Array(this.colNum).keys()];
  }

  get rows() {
    return [...Array(Math.ceil(this.limit / this.colNum)).keys()];
  }

  get dbItems(): Product[] {
    return this.items.slice(this.page * this.limit, (this.page + 1) * this.limit);
  }

  constructor(
    private productService: ProductService,
    private cartService: CartService,
  ) {
    this.productService.getProducts()
    .subscribe(
      (products: Product[]) => {
        this.items = products;
      }
    )
   }

  ngOnInit(): void {
    
  }

  addProduct(product: Product): void {
    // alert("Add to cart");
    // this.cartService.addProduct(product);
  }

  search(term: string): void {
    this.productService.getProducts(term)
    .subscribe(
      (products: Product[]) => {
        this.items = products;
        this.page = 0;
      }
    )
  }
}
