import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { IndexModule } from './index/index.module';
import { UserNavigationComponent } from './user-navigation/user-navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { HomepageComponent } from './homepage/homepage.component';
import { CommonComponentModule } from '../common-component/common-component.module';
import { CartBadgeComponent } from './cart/cart-badge/cart-badge.component';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { CartModule } from './cart/cart.module';

@NgModule({
  declarations: [
    UserNavigationComponent, 
    HomepageComponent,
    CartBadgeComponent,
  ],
  imports: [
    CommonModule,
    CommonComponentModule,
    CartModule,

    IndexModule,

    UserRoutingModule,

    LayoutModule,

    MatToolbarModule,

    MatButtonModule,

    MatSidenavModule,

    MatIconModule,

    MatListModule,

    MatInputModule,

    MatSelectModule,

    MatRadioModule,

    MatCardModule,

    ReactiveFormsModule,
  ],
  exports: [
  ]
})
export class UserModule { }
