import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonComponentModule } from 'src/app/common-component/common-component.module';
import { CartItemComponent } from './cart-item/cart-item.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { CartComponent } from './cart.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CartComponent,
    OrderDetailsComponent,
    CartItemComponent, 
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CommonComponentModule,
  ]
})
export class CartModule { }
