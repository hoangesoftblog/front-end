import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/cart-service/cart.service';

@Component({
  selector: 'app-cart-badge',
  templateUrl: './cart-badge.component.html',
  styleUrls: ['./cart-badge.component.css']
})
export class CartBadgeComponent implements OnInit {
  get length() {
    return this.cartService.length;
  }

  constructor(
    private cartService: CartService,
  ) {
   }

  ngOnInit(): void {
  }

}
