import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CartService } from 'src/app/cart-service/cart.service';
import { OrderService } from 'src/app/order-mgmt/order.service';
import { VoucherService } from 'src/app/voucher-management/voucher.service';
import { Order, Product, Voucher } from 'src/assets/models';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
    items: Product[] = [];
    availableVouchers: Voucher[] = [];
    // vouchers: Voucher[] = [];
    
    voucherArrayForm: FormGroup = this.fb.group({
        arr: this.fb.array([], [], [])
    });

    constructor(
        private cartService: CartService,
        private voucherService: VoucherService,
        private fb: FormBuilder,
        private orderService: OrderService,
    ) {
        this.items = this.cartService.items;
        this.voucherService.getVouchers()
        .pipe()
        .subscribe((vouchers: Voucher[]) => {
            this.availableVouchers = vouchers;
        })
    }

    ngOnInit(): void {
    }

    calculateTotal(): number {
        let totalPrice = this.allItemsPrice;

        totalPrice -= (this.arr.value as Voucher[])
            .map((v: Voucher): number => v ? this.getTrueDiscount(v) : 0)
            .reduce((total: number, current: number) => (total + current), 0);

        return totalPrice;
    }

    private get allItemsPrice(): number {
        return this.items
            .map((p: Product): number => (p.variants[0].price))
            .reduce((total: number, current: number) => (total + current), 0);
    }

    getTrueDiscount(v: Voucher): number {
        return v ? (Math.min(this.allItemsPrice * v.maxPercentage / 100, v.maxDiscountInVND)) : 0; 
    }


    get arr(): FormArray {
        return this.voucherArrayForm.get("arr") as FormArray;
    }

    add(): void {
        let v = this.generateVoucherControl();
        this.arr.push(v);
    }

    private generateVoucherControl(): FormControl {
        return this.fb.control(null, [Validators.required], []);
    }

    deleteVoucher(i: number): void {
        this.arr.removeAt(i);
    }

    compareVoucher(v1: Voucher, v2: Voucher): boolean {
        return v1 && v2 ? v1.id === v2.id : v1 === v2;
    }


    checkout(orderDetail: {deliveryTime: Date, deliveryAddr: string, note: string}): void {
        if (this.voucherArrayForm.valid) {
            if (this.items.length >= 1) {
                const order: any = {
                    ...orderDetail,
                    items: this.items,
                    vouchers: this.voucherArrayForm.value,
                    done: false,
                }
    
                console.log(order);
                
                this.orderService.add(order)
                .pipe()
                .subscribe((order: Order) => {
                   alert("Thank you for your order.")
    
                    window.location.assign("");
                })
            }
            else {
                alert("Please add a product(s) to your order.")
            }
        }
        else {
            alert("Please complete the order detail.");
        }
    }
}
