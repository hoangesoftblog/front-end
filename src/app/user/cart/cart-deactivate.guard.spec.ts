import { TestBed } from '@angular/core/testing';

import { CartDeactivateGuard } from './cart-deactivate.guard';

describe('CartDeactivateGuard', () => {
  let guard: CartDeactivateGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CartDeactivateGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
