import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';
import { ProductDetailComponent } from '../product-management/product-detail/product-detail.component';
import { CartDeactivateGuard } from './cart/cart-deactivate.guard';
import { CartComponent } from './cart/cart.component';
import { IndexComponent } from './index/index.component';
import { UserNavigationComponent } from './user-navigation/user-navigation.component';


let a: Route
const routes: Routes = [
  // {
  //   path: "",
  //   redirectTo: "index",
  //   pathMatch: "full",
  // },
  {
    path: "",
    component: UserNavigationComponent,
    data: {title: "Homepage", breadcrumb: "Home"},
    // redirectTo: "index",
    children: [
      {
        path: "index",
        component: IndexComponent,
      },
      {
        path: "cart",
        component: CartComponent,
        
        data: {title: "Cart", breadcrumb: "Cart"},
        canDeactivate: [CartDeactivateGuard]
      },
      {
        path: "product/:id",
        component: ProductDetailComponent,
        data: {title: "Product - :id", breadcrumb: "Item {id}"}
      },
      {
        path: "", redirectTo: "index", pathMatch: "full",
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
