import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountManangementComponent } from './account-manangement/account-manangement.component';
import { AdminLoginComponent } from './auth/admin-login/admin-login.component';
import { AdminComponent } from './admin/admin.component';
import { AuthGuard } from './auth/auth.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductManagementComponent } from './product-management/product-management.component';
import { StoreManagementComponent } from './store-management/store-management.component';
import { VoucherManagementComponent } from './voucher-management/voucher-management.component';
import { CurrentlyLoggedInAuthGuard } from './auth/currently-logged-in-auth.guard';
import { OrderMgmtComponent } from './order-mgmt/order-mgmt.component';
import { IndexComponent } from './user/index/index.component';

const routes: Routes = [
    {
        path: "login",
        component: AdminLoginComponent,
        data: { title: "Admin - Login" },
        canActivate: [CurrentlyLoggedInAuthGuard]
    },
    // {
    //     path: "user",
    //     component: AdminComponent,
    // },    
    {
        path: "**", 
        component: PageNotFoundComponent, 
        data: {
            title: "Error"
        }
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
