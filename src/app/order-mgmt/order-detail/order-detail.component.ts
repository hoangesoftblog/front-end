import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { VoucherService } from 'src/app/voucher-management/voucher.service';
import { Order, Product, Voucher } from 'src/assets/models';

@Component({
    selector: 'app-order-detail',
    templateUrl: './order-detail.component.html',
    styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {
    @Input() order!: Order;

    vouchers: Voucher[] = [];

    constructor(
        private activeModal: NgbActiveModal,
        private router: Router,
        private voucherService: VoucherService,
    ) {
        this.voucherService.getVouchers().pipe(

        )
        .subscribe(
            (vouchers: Voucher[]) => {
                this.vouchers = vouchers;
            }
        )
    }

    ngOnInit(): void {
    }

    cancel(): void {
        this.activeModal.dismiss("Voucher detail - close");
    }

    closeModal(): void {
        // this.router.navigate(["/product", item.id]);
        this.activeModal.dismiss();
    }

    get totalPrice(): number {
        let totalPrice = this.order.items
            .map((p: Product): number => (p.variants[0].price))
            .reduce((total: number, current: number) => (total + current), 0);

        totalPrice -= this.order.vouchers
            .map((v: Voucher): number => (Math.min(totalPrice * v.maxPercentage / 100, v.maxDiscountInVND)))
            .reduce((total: number, current: number) => (total + current), 0);

        return totalPrice;
    }
}
