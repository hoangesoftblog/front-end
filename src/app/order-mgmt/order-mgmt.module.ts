import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderMgmtComponent } from './order-mgmt.component';
import { CommonComponentModule } from '../common-component/common-component.module';
import { OrderFormComponent } from './order-form/order-form.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DateValueAccessorModule } from 'angular-date-value-accessor';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    OrderMgmtComponent,
    OrderFormComponent,
    OrderDetailComponent,
  ],
  imports: [
    ReactiveFormsModule,

    CommonModule,
    CommonComponentModule,
    DateValueAccessorModule,
    RouterModule,
  ]
})
export class OrderMgmtModule { }
