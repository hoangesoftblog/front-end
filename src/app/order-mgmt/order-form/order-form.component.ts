import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Order, Product, Voucher } from "src/assets/models";
import { map } from 'rxjs';
import { ProductService } from 'src/app/product-management/product.service';
import { VoucherService } from 'src/app/voucher-management/voucher.service';
import { OrderService } from '../order.service';

@Component({
    selector: 'app-order-form',
    templateUrl: './order-form.component.html',
    styleUrls: ['./order-form.component.css']
})
export class OrderFormComponent implements OnInit {
    @Input() order?: Order;

    form: FormGroup;
    products: Product[] = [];
    vouchers: Voucher[] = [];

    constructor(
        private orderService: OrderService,
        private activeModal: NgbActiveModal,
        private fb: FormBuilder,

        private voucherService: VoucherService,
        private productService: ProductService,
    ) {
        this.form = this.fb.group({
            deliveryAddr: ["", [Validators.required]],
            deliveryTime: [new Date(), [Validators.required]],
            note: ["", [], []],
            items: this.fb.array([], [Validators.required, this.arrayOfNonNullValidator]),
            vouchers: this.fb.array([]),
        });

        this.productService.getProducts().pipe(
            map((data: any) => {
                return this.productService.retrieveMultiple(data);
            }),
        ).subscribe(
            (products: Product[]) => {
                console.log("Fetch products", products);
                this.products = products;
            }
        );

        this.voucherService.getVouchers()
        .pipe(
           map((vouchers: Voucher[]): Voucher[] => {
               return vouchers.slice()
               .sort((v1: Voucher, v2: Voucher) => -(v1.maxDiscountInVND - v2.maxDiscountInVND))
               .sort((v1: Voucher, v2: Voucher) => -(v1.maxPercentage - v2.maxPercentage))
           }),
        )
        .subscribe(
            (v: any): void => {
                console.log("Fetch vouchers", v);
                this.vouchers = v;
            }
        );

        this.items.valueChanges
        .subscribe(
            // Figured after inspection
            (val: Product[]) => {
                // // console.log(val);
                // this.validVouchers = this.vouchers
                // // .filter((v: Voucher) => v.minProductsRequired <= val.length)
                // .map((v: Voucher) => ({voucher: v, allowed: v.minProductsRequired <= val.length}));
            }
        )
    }

    arrayOfNonNullValidator(control: AbstractControl): ValidationErrors | null {
        let tempControl = control as FormArray;
        return [...Array(tempControl?.length ?? 0).keys()]
        .map((key: number): AbstractControl | null => tempControl.get(key.toString()))
        .every((e: AbstractControl | null) => {return (e?.value !== null)}) ? null : {arrayOfNonNull: true};
    }

    private parseOrderForEdit(o: Order) {
        let tempOrder = {...o, deliveryTime: new Date(o.deliveryTime), };
        console.log("Temp order:",tempOrder);
        return tempOrder;
    }

    ngOnInit(): void {
        if (this.order) {
            let tempOrder = this.parseOrderForEdit(this.order);
            this.form.patchValue(tempOrder);
            
            // Patch form array
            // https://stackoverflow.com/q/65292461
            this.order.items.forEach((p: Product) => {
                let group = this.generateItemGroup();
                group.get("product")?.patchValue(p);
                this.items.push(group);
            });

            this.order.vouchers.forEach((v: Voucher) => {
                let group = this.generateVoucherGroup();
                group.patchValue(v);
                this.formVouchers.push(group);
            })
        }
    }

    private returnOrderToSubmit(o: Object): Order {
        let tempOrder = {...o, 
            deliveryTime: ((o as any)["deliveryTime"] as Date), 
            done: ((o as any)["done"] as boolean) ?? false,
            items: ((o as any)["items"] as {product: Product}[]).map((val: {product: Product}) => val.product),
        } as Order;
        return tempOrder;
    }

    submit(): void {
        console.log(this.items.errors);

        if (this.form.valid) {
            let order = {...(this.order ?? {}), ...this.form.value} as Order;
            order = this.returnOrderToSubmit(order);
            console.log("Finished order:", order);
            this.activeModal.close(order);
        }
    }
    

    cancel(): void {
        this.activeModal.dismiss("Order form - close");
    }


    compareProduct(p1: Product, p2: Product) {
        return p1 && p2 ? p1.id === p2.id : p1 === p2;
    }

    compareVoucher(v1: Voucher, v2: Voucher) {
        return v1 && v2 ? v1.id === v2.id : v1 === v2;
    }


    get items() {
        let items = this.form.get("items") as FormArray;
        return items;
    }

    addItem(): void {
        let group = this.generateItemGroup();
        this.items.push(group);
    }

    private generateItemGroup(): AbstractControl {
        return this.fb.group({
            product: [null, [Validators.required],],
            // amount: [1, [Validators.required, Validators.min(1)]],
        });
        // return this.fb.control(
        //     null, [], []
        // )
    }

    deleteItem(i: number) {
        this.items.removeAt(i);
    }

    get formVouchers() {
        return this.form.get("vouchers") as FormArray;
    }

    addVoucher(): void {
        // Only allow 1 vouchers, to easy implement
        if (this.formVouchers.length < 1) {
            this.formVouchers.push(this.generateVoucherGroup());
        }
    }

    private generateVoucherGroup(): AbstractControl {
        return this.fb.control(
            null, [], []
        );
    }

    deleteVoucher(i: number) {
        this.formVouchers.removeAt(i);
    }
}
