import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Order } from 'src/assets/models';
import { UserInfoService } from '../auth/user-info.service';
import { RowAction, TableColumn } from '../common-component/table/table.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { OrderFormComponent } from './order-form/order-form.component';
import { OrderService } from './order.service';

@Component({
    selector: 'app-order-mgmt',
    templateUrl: './order-mgmt.component.html',
    styleUrls: ['./order-mgmt.component.css']
})
export class OrderMgmtComponent implements OnInit {
    orders: Order[] = [];

    get dbOrders(): Order[] {
        return this.orders.slice(this.page * this.limit, (this.page + 1) * this.limit);
    }

    @ViewChild("cancelDialog") cancelDialog?: TemplateRef<any>;

    page: number = 0;
    private limit: number = 10;
    columns: TableColumn[] = [
        {title: "Order ID", property: "id"},
        {title: "Delivery Address", property: "deliveryAddr"},
        {title: "Delivery Time", property: "deliveryTime"},
        {title: "Items in order", property: "items", displayAs: (order: Order) => (order.items.length)},
        {title: "Delivered", property: "done", displayAs: (order: Order) => order.done.toString().toUpperCase()},
        {title: "Cancelled", property: "cancel", displayAs: (order: Order) => order.cancel.toString().toUpperCase()},
    ];
    rowActions: RowAction[] = [
        {title: "View", action: this.openOrderDetail.bind(this)},
        ...(this.authService.currentlyLoggedIn?.role === "Admin" ? 
        [
            {title: "Edit", action: this.openEditOrder.bind(this)},
            {title: "Cancel", action: this.openCancelDialog.bind(this)}
        ] : 
        []),
        
    ];

    /**
     * Get the last page, counting from 0
     */
    get lastPage(): number {
        if (!this.orders.length) return 0;
        else return Math.floor((this.orders.length - 1) / this.limit); 
    }

    constructor(
        private orderService: OrderService,
        private modalService: NgbModal,
        private authService: UserInfoService,
    ) {
        this.orderService.get()
        .pipe(

        )
        .subscribe(
            (orders: Order[]) => {
                this.orders = orders;
            }
        );
    }

    ngOnInit(): void {
        // this.openAddOrder();
    }

    onPageChange(page: number): void {
        this.page = page;
    }

    search(term: string): void {
        this.orderService.get(term)
        .pipe(

        )
        .subscribe((orders: Order[]) => {
            this.page = 0;
            this.orders = orders;
        });
    }

    refresh() {
        console.log("Refreshing");
        this.orderService.get()
        .pipe(

        )
        .subscribe((orders: Order[]) => {
            this.page = 0;
            this.orders = orders;
        });
    }

    openAddOrder() {
        this.modalService.open(OrderFormComponent).result.then(
            (val: Order) => {
                this.orderService.add(val).pipe(
                    
                )
                .subscribe(
                    (order: Order) => {
                        this.orders.push(order);
                    }
                )
            },
            (reason: ModalDismissReasons | string) => {

            } 
        )
    }

    openOrderDetail(order: Order): void {
        let ref = this.modalService.open(OrderDetailComponent);
        ref.componentInstance.order = order;
        ref.result.then(
            (order: Order) => {
                
            },
            (reason: ModalDismissReasons | string) => {
                
            }
        )
    }

    openEditOrder(order: Order): void {
        let ref = this.modalService.open(OrderFormComponent);
        ref.componentInstance.order = order;
        ref.result.then(
            (order: Order) => {
                this.orderService.update(order)
                .subscribe(
                    () => {
                        if (order) {
                            for (let i = 0; i < this.orders.length; i++) {
                                if (order.id === this.orders[i].id) {
                                    console.log("Update product, match with id:", this.orders[i].id)
                                    this.orders[i] = order;
                                    break;
                                }
                            }
                        }
                    }    
                )
            },
            (reason: ModalDismissReasons | string) => {
                
            }
        )
    }


    selection!: SelectionModel<any>;
    get allSelected(): Order[] {
        return this.selection?.selected ?? [];
    }
    setAllSelected(selection: SelectionModel<any>): void {
        this.selection = selection;
        // console.log(this.allSelectedProducts);
    }

    openDeleteDialog(modal: any): void {
        this.modalService.open(modal).result.then(
            (val) => {
                let successfully = true;
                console.log("Delete user - close. Confirm delete");
                this.selection.selected.forEach((order: Order) => {
                    this.orderService.delete(order.id)
                    .pipe(
                        // catchError((err, caught: Observable<User>) => {return of([])})
                    )
                    .subscribe(
                        // user in this is null
                        (tempUser: void) => {
                            console.log("Delete voucher,", tempUser);
                            this.orders = (this.orders.filter(o => o.id !== order.id));
                        }
                    )
                });
                this.selection.clear();
            },
            (err) => {
                
            }
        )
    }


    openCancelDialog(order: Order) {
        if (this.cancelDialog) {
            this.modalService.open(this.cancelDialog).result.then(
                () => {
                    order.cancel = true;
                }, (err) => {
                    
                }
            )
        }
    }
}
