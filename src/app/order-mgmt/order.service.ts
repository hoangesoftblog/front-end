import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, tap } from 'rxjs';
import RetrieveData from 'src/assets/common-interface';
import { Order } from 'src/assets/models';
import { environment } from 'src/environments/environment';
import { CRUDInterface } from '../crud-api.interface';
const baseAPI = environment.baseAPI;

@Injectable({
    providedIn: 'root'
})
export class OrderService implements RetrieveData, CRUDInterface<Order> {
    url = baseAPI + "api/orders";
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    constructor(
        private http: HttpClient,
    ) { }

    get(term?: string, sortBy?: string, order?: "asc" | "desc" | "none", page?: number, limit?: number): Observable<Order[]> {
        if (!environment.production) {
            // If all arguments are null, then just fetch all the database
            if ([...arguments].every(e => !e)) {
                return this.http.get<Order[]>(this.url).pipe(
                    map((orders: any) => this.retrieveMultiple(orders))
                );
            }

            // Else, try to search
            // The data here is in correct form of Order[]
            let ordersObservable = of<Order[]>([]);
            if (term) {
                ordersObservable = ordersObservable.pipe(
                    tap((orders: Order[]) => { console.log("Get products, length:", orders.length) }),
                    map((orders: Order[]) => orders.filter((order) => (Object.values(order).some((val) => ((String(val).toLowerCase()).includes(term.toLowerCase())))))),
                )
            }

            return ordersObservable;
        }
        else {
            const urlSearchParams: URLSearchParams = Object
            .entries({ term, sortBy, order, page, limit })
            .filter(([key, value]) => value)
            .reduce(
                (obj: URLSearchParams, [key, value]: [string, any]) => {
                    obj.append(key, value);
                    return obj;
                }, new URLSearchParams()
            );

            return this.http.get<Order[]>(this.url + `/${urlSearchParams}`)
            .pipe();
        }
    }

    add(order: Order): Observable<Order> {
        return this.http.post<Order>(this.url, order, this.httpOptions)
            .pipe(
                map((data: any) => this.retrieveSingle(data))
            )
    }

    update(order: Order): Observable<void> {
        return this.http.put<void>(this.url, order, this.httpOptions)
            .pipe();
    }

    delete(id: number): Observable<void> {
        const url = `${this.url}/${id}`;

        return this.http.delete<void>(url, this.httpOptions)
            .pipe()
            ;
    }


    retrieveMultiple(data: any): Order[] {
        if (Array.isArray(data)) {
            return data;
        }
        else {
            const users = (data["data"] as Order[]);
            return users;
        }
    }

    retrieveSingle(data: any): Order {
        if (data["data"]) return data["data"] as Order;
        else return data;
    }


    getOne(id: number): Observable<Order> {
        return this.http.get<Order>(this.url + `/${id}`)
            .pipe();
    }
}
