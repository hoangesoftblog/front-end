import { Component, OnInit } from '@angular/core';
import { MatChip, MatChipSelectionChange } from '@angular/material/chips';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { catchError, map, Observable, of, switchMap, tap } from 'rxjs';
import { CartService } from 'src/app/cart-service/cart.service';
import { Product } from 'src/assets/models';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  product$!: Observable<Product>;
  selectedVariant: {size: string; price: number;} | undefined = undefined;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private productService: ProductService,
    public cartService: CartService,
  ) { }

  ngOnInit(): void {
    this.product$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.productService.getProduct(Number(params.get("id") ?? 1))),
      // catchError((err, caught: Observable<Product>) => of(null)),
      map((data: any) => this.productService.retrieveSingle(data) as Product),
      tap((product: Product) => {
        // console.log(product);
        // if (!product) {
        //   this.router.navigate(["/abc"], {skipLocationChange: true})
        // }
      })
    )
  }

  updateSelection(change: MatChipSelectionChange) {
    // console.log(change);
  }

  changeVariant(variant: {size: string; price: number;}){
    if (this.selectedVariant) {
      if (this.selectedVariant === variant) {
        this.selectedVariant = undefined;
      }
      else {
        this.selectedVariant = variant;
      }
    }
    else {
      this.selectedVariant = variant;
    }
  }

  toggleSelection(chip: MatChip) {
    chip.toggleSelected();
  }
}
