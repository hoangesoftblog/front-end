import { SelectionChange, SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { catchError, tap } from 'rxjs';
import { Product } from 'src/assets/models';
import { RowAction, TableColumn } from '../common-component/table/table.component';
import { AddProductComponent } from './add-product/add-product.component';
import { ProductService } from './product.service';
import { UpdateProductComponent } from './update-product/update-product.component';

@Component({
    selector: 'app-product-management',
    templateUrl: './product-management.component.html',
    styleUrls: ['./product-management.component.css']
})
export class ProductManagementComponent implements OnInit {

    constructor(
        private productService: ProductService,
        private modalService: NgbModal,
    ) { }

    // For delete and update
    selectedProduct!: Product

    dbProducts: Product[] = [];
    products: Product[] = [];

    private limit = 10;

    columns = ["name", "image", "category", "size"]
    columnNames = ["Name", "Image", "Category", "Size"]
    displayAs = [undefined, undefined, (p: Product) => p.category.name, this.getSize];
    sortable = [true, false, true, true];
    tableColumns: Array<TableColumn> = this.columns.map((val, index) => ({
        title: this.columnNames[index],
        property: val,
        sortable: this.sortable[index],
        displayAs: this.displayAs[index],
    }))

    rowActions: RowAction[] = [
        {title: "Edit", action: this.openUpdateDialog.bind(this),},
        // {title: "Delete", action: this.openDeleteDialog.bind(this), }, 
    ]

    ngOnInit(): void {
        this.productService.getProducts()
            .subscribe((data) => {
                const products: Product[] = this.productService.retrieveMultiple(data);
                this.setProducts(products);
                // console.log(products[0].variants.map((val: {size: string, price: number}) => val.size).join(", "));
            });

        // this.openAddDialog();
    }

    private setProducts(products: Product[]) {
        this.products = products;
        this.updateDisplayProducts();
    }

    setSelectedProduct(product: Product) {
        this.selectedProduct = product;
    }

    search(term: string): void {
        this.productService.getProducts(term).pipe(
            // catchError((err) => ),

        ).subscribe((d: any) => {
            // Reset the page
            this.page = 0;
            this.setProducts(this.productService.retrieveMultiple(d));
        })
    }

    page: number = 0;
    get lastPage(): number {
        if (!this.products.length) return 0;
        else return Math.floor((this.products.length - 1) / this.limit);
    }

    private updateDisplayProducts() {
        this.dbProducts = this.products.slice(this.page * this.limit, (this.page + 1) * this.limit);
    }

    onPageChange(page: number) {
        this.page = page;
        this.updateDisplayProducts();
    }

    openAddDialog() {
        let ref = this.modalService.open(AddProductComponent);
        ref.result.then(
            (val) => {
                this.products.push(val);
                this.setProducts(this.products);
            },
            (err) => {
                // console.log("Modal AddUser error");
                // console.log(err);
            }
        )
    }

    openUpdateDialog(product?: Product) {
        let ref = this.modalService.open(UpdateProductComponent);
        ref.componentInstance.product = product;
        ref.result.then(
            (product) => {
                if (product) {
                    for (let i = 0; i < this.products.length; i++) {
                        if (product.id === this.products[i].id) {
                            // console.log("Update product, match with id:", this.products[i].id)
                            this.products[i] = product;
                            break;
                        }
                    }

                    this.setProducts(this.products);
                }
            },
            (err) => {
                // console.log("Modal updateProduct error");
                // console.log(err);
            }
        )
    }

    refresh() {
        this.productService.getProducts()
            .pipe(
                // catchError((err: any, caught: Observable<User[]>) => ({})),
                tap((products) => {
                    // console.log("Refresh page", products);
                })
            )
            .subscribe((d: any) => {
                // Reset the page
                this.page = 0;
                this.setProducts(this.productService.retrieveMultiple(d))
            });
    }

    getSize(product: Product): string {
        let printed = product.variants.map((variant: {size: string, price: number}) => variant.size).join(", ");
        return printed;
    }
    


    selection!: SelectionModel<any>;
    get allSelected(): Product[] {
        return this.selection?.selected ?? [];
    }
    setAllSelected(selection: SelectionModel<any>): void {
        this.selection = selection;
        // console.log(this.allSelectedProducts);
    }
    openDeleteDialog(modal: any) {
        this.modalService.open(modal).result.then(
            (val) => {
                // // console.log("Delete user - close. Confirm delete");
                // let product: Product = this.selectedProduct;
                // // this.users = this.users.filter(u => u.id !== user.id);
                // this.productService.deleteProduct(product.id)
                //     .pipe(
                //         // catchError((err, caught: Observable<User>) => {return of([])})
                //     )
                //     .subscribe(
                //         // user in this is null
                //         () => { this.setProducts(this.products.filter(p => p.id !== product.id)); }
                //     )


                // // TODO: This function is implemented SUPER WRONG!!!
                // // Required a re-work
                (this.allSelected as Product[]).forEach(product => {
                    this.productService.deleteProduct(product.id)
                    .subscribe(
                        () => {
                            this.products = this.products.filter(p => p.id !== product.id);
                        }
                    )
                });
                // Clean-up
                this.setProducts(this.products);
                this.selection?.clear();
            },
            (reason) => {
                // console.log("Delete user - closed.", reason);
            }
        )
    }
}
