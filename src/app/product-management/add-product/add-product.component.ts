import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { CategoriesService } from 'src/app/categories.service';
import { Category, Product } from 'src/assets/models';
import { ProductService } from '../product.service';

@Component({
    selector: 'app-add-product',
    templateUrl: './add-product.component.html',
    styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
    @Output() emitter = new EventEmitter<Product>();

    categories!: Category[];

    form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private productService: ProductService,
        private categoryService: CategoriesService,
        private activeModal: NgbActiveModal,
    ) {
        this.form = this.fb.group({
            name: ["", [Validators.required, Validators.min(5)]],
            category: [, [Validators.required]],
            variants: this.fb.array([
                this.fb.group({
                    size: ["", [Validators.required]],
                    price: [0, [Validators.required]],
                }
                )
            ], [Validators.minLength(1)]),
            description: [""],
        });
    }

    get variants(): FormArray {
        return this.form.get("variants") as FormArray;
    }

    submit(): void {
        console.log("Add is value", this.form.valid);
        console.log("Current value is", this.form.value);
        if (this.form.valid) {
            const imgURL = this.uploadFile();
            let finishedUser: any = { ...this.form.value, image: imgURL };
            if (finishedUser) {
                this.productService.addProduct(finishedUser)
                    .subscribe((product: Product) => {
                        // console.log(content);
                        // // Return the user back to AccountManagementComponent
                        // this.userEmitter.emit(finishedUser);
                        this.activeModal.close(product);
                    })
            }
        }
    }

    cancel() {
        this.activeModal.dismiss("AddUser - cancel");
    }

    ngOnInit(): void {
        console.log("Add user show!");
        this.categoryService.getCategories()
            .pipe(

            )
            .subscribe((data: Category[]) => {
                this.categories = this.categoryService.retrieveCategories(data);
            })
    }

    addVariant() {
        this.variants.push(this.fb.group({
            size: ["", [Validators.required]],
            price: [0, [Validators.required]],
        }
        ));
    }

    deleteVariant(i: number): void {
        this.variants.removeAt(i);
    }

    compareCategory(c1: Category, c2: Category) {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    file?: File;
    get fileName(): string | undefined {
        return this.file?.name ?? undefined;
    } 
    handleFile(event: any) {
        // console.log(event.target);
        const file: File = event.target?.files[0];
        if (file) {
            this.file = file;
        }
    }

    uploadFile(): string | null {
        if (this.file) {
            // const formData = new FormData();
            // formData.append("thumbnail", this.file);
            // // const upload$ = this.http.post("/api/thumbnail-upload", formData);
            // // upload$.subscribe();
            return "http://lorempixel.com/g/320/200/nightlife/";
        }
        else return null; 
    }
}
