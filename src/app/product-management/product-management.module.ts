import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateProductComponent } from './update-product/update-product.component';
import { AddProductComponent } from './add-product/add-product.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductService } from './product.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonComponentModule } from '../common-component/common-component.module';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductManagementRoutingModule } from './product-management-routing.module';
import { ProductManagementComponent } from './product-management.component';



@NgModule({
  declarations: [
    UpdateProductComponent,
    AddProductComponent,
    ProductDetailComponent,
    ProductManagementComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CommonComponentModule,
    ProductManagementRoutingModule,
  ],
  providers: [
    ProductService,
  ]
})
export class ProductManagementModule { }
