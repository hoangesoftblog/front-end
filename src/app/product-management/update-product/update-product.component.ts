import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoriesService } from 'src/app/categories.service';
import { Category, Product } from 'src/assets/models';
import { ProductService } from '../product.service';

@Component({
    selector: 'app-update-product',
    templateUrl: './update-product.component.html',
    styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {
    @Input() product!: Product;
    categories!: Category[];

    form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private productService: ProductService,
        private categoryService: CategoriesService,
        private activeModal: NgbActiveModal,
    ) {
        this.form = this.fb.group({
            name: ["", [Validators.required, Validators.min(5)]],
            category: [, [Validators.required]],
            variants: this.fb.array([], []),
            description: [""],
        });
    }

    get variants(): FormArray {
        return this.form.get("variants") as FormArray;
    }

    submit(): void {
        console.log("Add is value", this.form.valid);
        if (this.form.valid) {
            let finishedProduct: any = { ...this.product, ...this.form.value };
            if (finishedProduct) {
                this.productService.updateProduct(finishedProduct)
                    .subscribe((returnProduct: void) => {
                        console.log("Finsh product", finishedProduct);
                        // // Return the user back to AccountManagementComponent
                        // this.userEmitter.emit(finishedUser);
                        this.activeModal.close(finishedProduct);
                    })
            }
        }
    }

    cancel() {
        this.activeModal.dismiss("AddUser - cancel");
    }

    ngOnInit(): void {
        console.log("Add user show!");
        this.categoryService.getCategories()
            .pipe(

            )
            .subscribe((data: any) => {
                this.categories = data;
            });

        console.log("Editing product:", this.product);
        this.form.patchValue({ ...this.product });
        this.product.variants.forEach(
            (variant: {size: string, price: number}) => {
                let group = this.generateVariant();
                group.patchValue(variant);
                this.variants.push(group);
            }
        )
    }

    addVariant() {
        this.variants.push(this.generateVariant());
    }

    generateVariant() {
        return this.fb.group({
            size: ["", [Validators.required]],
            price: [0, [Validators.required]],
        });
    }

    deleteVariant(i: number): void {
        this.variants.removeAt(i);
    }


    compareCategory(c1: Category, c2: Category) {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }
}
