import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, tap } from 'rxjs';
import RetrieveData from 'src/assets/common-interface';
import { products } from 'src/assets/dataset';
import { Product } from 'src/assets/models';

import { environment } from 'src/environments/environment';
const baseAPI = environment.baseAPI;


@Injectable({
  providedIn: 'root'
})
export class ProductService implements RetrieveData {
  url = baseAPI + "api/products";
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient
  ) { }


  /**
     * 
     * @param term Search keyword
     * @param sortColumn 
     * @param order 
     * @param page 
     * @param limit 
     * @returns 
     */
  getProducts(term?: string, sortColumn?: string, order?: "asc" | "desc", page?: number, limit?: number): Observable<Product[]> {
    // If all arguments are null, then just fetch all the database
    if ([...arguments].every(e => !e)) {
      return this.http.get<Product[]>(this.url)
      .pipe(
        map((data: any) => this.retrieveMultiple(data))
      );
      ;
    }

    // Else, try to search
    let productsObservable = of<Product[]>(products);
    if (term) {
      productsObservable = productsObservable.pipe(
        tap((products: Product[]) => { console.log("Get products, length:", products.length) }),
        map((products: Product[]) => products.filter((product) => (Object.values(product).some((val) => ((String(val).toLowerCase()).includes(term.toLowerCase())))))),
      )
    }

    if (sortColumn) {
      productsObservable = productsObservable.pipe(
        map((users) => {
          return users.slice()
            .sort((a: Product, b: Product): number => {
              if (sortColumn && Object.keys(a).includes(sortColumn)) return (((a as any)[sortColumn] as string).localeCompare((b as any)[sortColumn] as string))
              // Keeps in the same order
              else return 1;
            })
        })
      );
    }

    return productsObservable.pipe(
      map((data: any) => this.retrieveMultiple(data))
    );
  }

  retrieveMultiple(data: any): Product[] {
    if (Array.isArray(data)) {
      return data;
    }
    else {
      const users = (data["data"] as Product[]);
      return users;
    }
  }

  retrieveSingle(data: any): Product{
    if (data?.["data"]) {
      return data["data"];
    }
    else return data;
  }

  getProduct(id: number): Observable<Product> {
    const url = `${this.url}/${id}`;
    return this.http.get<Product>(url).pipe(

    );
  }

  
  addProduct(product: Product): Observable<Product> {
    return this.http.post<Product>
    (this.url, product, this.httpOptions)
    .pipe(
      map((data: any) => this.retrieveSingle(data)),
      tap((product: Product) => {
        console.log(products);
      })
    );
  }


  updateProduct(product: Product): Observable<void> {
    return this.http.put<void>(this.url, product, this.httpOptions)
    .pipe(
      // map((data: any) => this.retrieveSingle(data)),
      // tap((product: Product) => {
      //   console.log("Product after update:", product);
      // })
    );
  }

  deleteProduct(id: number): Observable<void> {
    const url = `${this.url}/${id}`;

    return this.http.delete<void>(url, this.httpOptions)
    .pipe(
        tap(_ => console.log(`deleted product id=${id}`)),
    );
  }
}
