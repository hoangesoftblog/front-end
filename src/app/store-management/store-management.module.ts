import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreManagementComponent } from './store-management.component';



@NgModule({
  declarations: [
    StoreManagementComponent,
  ],
  imports: [
    CommonModule
  ]
})
export class StoreManagementModule { }
