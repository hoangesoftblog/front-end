import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AccountManangementComponent } from "../account-manangement/account-manangement.component";
import { AuthGuard } from "../auth/auth.guard";
import { OrderMgmtComponent } from "../order-mgmt/order-mgmt.component";
import { ProductManagementComponent } from "../product-management/product-management.component";
import { StoreManagementComponent } from "../store-management/store-management.component";
import { VoucherManagementComponent } from "../voucher-management/voucher-management.component";
import { AdminComponent } from "./admin.component";

const routes: Routes = [
    {
        path: "admin",
        component: AdminComponent,
        data: { title: "Admin", breadcrumb: "Admin" },
        canActivate: [AuthGuard],
        children: [
            {path: "", redirectTo: "account-mgmt", pathMatch: "full"},
            {
                path: "account-mgmt",
                component: AccountManangementComponent,
                data: { title: "Admin - Account Management", breadcrumb: "Account Management" },
                // loadChildren: "./account-manangement/account-manangement.module#AccountManangementModule"
            },
            {
                path: "store-mgmt",
                component: StoreManagementComponent,
                data: { title: "Admin - Store Management", breadcrumb: "Store Management" }
            },
            {
                path: "product-mgmt",
                component: ProductManagementComponent,
                data: { title: "Admin - Product Management", breadcrumb: "Product Management" }
            },
            {
                path: "voucher-mgmt",
                component: VoucherManagementComponent,
                data: { title: "Admin - Voucher Management", breadcrumb: "Voucher Management" }
            },
            {
                path: "order-mgmt",
                component: OrderMgmtComponent,
                data: { title: "Admin - Order Management", breadcrumb: "Order Management" }
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class AdminRoutingModule { }

