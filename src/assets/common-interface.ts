/**
 * Interface using with ImMemoryWebAPI,
 * since the data return is nested in an new object
 */
export default interface RetrieveData {
    retrieveMultiple(...args: any[]): any;

    retrieveSingle(...args: any[]): any;
}