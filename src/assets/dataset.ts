import { LoginInfo, Category, Product, User, Voucher } from "./models";
import {faker} from "@faker-js/faker"
import { Order } from "src/assets/models";


function range(num: Number) {
    return [...Array(num).keys()]
}

export const users: User[] = ([...Array(5).keys()].map(val => val + 1).map((index) => [
    { id: index, name: faker.name.findName(), gender: faker.name.gender(true), role: Math.random() > 0.5 ? "Admin" : "User", phone: faker.phone.phoneNumber(), email: faker.internet.email() },
]).flat() as User[]);

export const login: LoginInfo[] = ([
    { id: 1, username: "admin", password: "admin", role: "Admin", },
    { id: 2, username: "user", password: "user", role: "User", },
]);



export const categories: Category[] = ([...Array(5).keys()].map(val => val + 1).map(val => val + 1).map((index) => [
    { 
        id: index, 
        name: faker.commerce.product(), 
    },
]).flat() as Category[])

export const products: Product[] = ([...Array(25).keys()].map(val => val + 1).map((index) => [
    { 
        id: index, 
        name: faker.commerce.productName(), 
        image: faker.image.imageUrl(320, 240, "drink"), 
        category: categories[Math.floor(Math.random() * categories.length)], 
        variants: [{
            size: ["S", "M", "L"][Math.floor(Math.random() * 3)], 
            price: Number(faker.commerce.price()) * 1000,
        }],
        description: faker.commerce.productDescription(),
    },
]).flat() as Product[])


export const vouchers: Voucher[] = ([...Array(5).keys()].map(val => val + 1).map((index) => {
    return {
        id: index,
        code: faker.finance.bic(),
        start: faker.date.past(),
        end: faker.date.soon(),
        // limit: 100,
        maxPercentage: faker.datatype.number({min: 3, max: 10}),
        usageLeft: Math.floor(Math.random() * 100),
        maxDiscountInVND: parseInt(faker.finance.amount(5, 50)) * 1000,
        minProductsRequired: faker.datatype.number({min: 1, max: 3}),
        items: [
            products[faker.datatype.number({min: 0, max: products.length - 1})],
        ]
    }
})) as Voucher[];

export const orders: Order[] = ([...Array(4).keys()]).map(val => val + 1).map((index) => ({
    id: index,
    deliveryAddr: faker.address.city(),
    deliveryTime: faker.date.soon(),
    note: faker.lorem.lines(3),
    vouchers: [],
    items: [
        products[faker.datatype.number({min: 0, max: products.length - 1})
    ],
],
})).map((e: any) => ({...e, done: new Date(e["deliveryTime"]) < new Date(new Date().getTime() + 3 * 24 * 60 * 60 * 1000), cancel: false} as Order));