export interface LoginInfo {
    id: number,
    username: string,
    password: string,
    role: Role,
}

export interface LoginInfoWithToken extends LoginInfo {
    token: string,
}

export interface User {
    id: number,
    name: string,
    gender: Gender, 
    role: Role,
    phone: string,
    email: string,
}

export type Gender = "Male"|"Female"|"Other";
export type Role = "Admin"|"User";


export interface Category {
    id: number,
    name: string,
}

export interface ProductWithoutVouchers {
    id: number,
    name: string,
    category: Category,
    variants: {
        size: string,
        price: number,
    }[],
    image: string,
    description: string,
}

export interface Product extends ProductWithoutVouchers {
    // vouchers: Voucher[],
}

export interface VoucherWithoutItems {
    id: number,
    code: string,
    // Date
    start: Date,
    end: Date,
    
    // limit: number,
    usageLeft: number,
    maxPercentage: number,
    maxDiscountInVND: number,
    minProductsRequired: number,
}

export interface Voucher extends VoucherWithoutItems {
    items: Product[],
};


interface ProductWithAmount {
    product: Product,
    amount: number,
}

interface OldOrder {
    id: number,
    deliveryAddr: string,
    deliveryTime: Date,
    note: string,
    vouchers: Voucher[],
    items: Product[],
    done: boolean,
    cancel: boolean,
    // get total: () => number,
}

export class Order implements OldOrder {
    id!: number;
    deliveryAddr!: string;
    deliveryTime!: Date;
    note!: string;
    vouchers: Voucher[] = [];
    items: Product[] = [];
    done: boolean = false;
    cancel: boolean = false;

    public get total(): number {
        let totalPrice = this.items
            .map((p: Product): number => (p.variants[0].price))
            .reduce((total: number, current: number) => (total + current), 0);

        totalPrice -= this.vouchers
            .map((v: Voucher): number => (Math.min(totalPrice * v.maxPercentage / 100, v.maxDiscountInVND)))
            .reduce((total: number, current: number) => (total + current), 0);

        return totalPrice; 
    }    
}



// export enum Gender {
//     MALE = "Male",
//     FEMALE = "Female",
//     OTHER = "Other",
// }
// export enum Role {
//     ADMIN = "Admin",
//     USER = "User",
// }